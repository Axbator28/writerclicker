import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    score: 0,
    wordAmelioration: 1,
    ameliorations: [
      {
        name: "ink",
        description: "You need ink to write lol",
        cost: 10,
        level: 0,
        image: "pixel_hands-1373363_1920.png",
      },
      {
        name: "books",
        description: "You need INSPIRATION",
        cost: 100,
        level: 0,
        image: "pixel_books-1245690_1920.png",
      },
    ],
    ghostWriters: [
      {
        name: "ink",
        description: "You need ink to write lol",
        cost: 10,
        level: 0,
        image: "pixel_hands-1373363_1920.png",
      },
      {
        name: "books",
        description: "You need INSPIRATION",
        cost: 100,
        level: 0,
        image: "pixel_books-1245690_1920.png",
      },
    ],
  },
  getters: {
    scoreToIncrease: (state) => {
      let retour = state.wordAmelioration;
      for (let i = 0; i < state.ameliorations.length; i++) {
        retour = retour * 2 ** state.ameliorations[i].level;
      }
      return retour;
    },
  },
  mutations: {
    INCREASE_SCORE(state, amount = 1) {
      state.score += Number(amount);
    },
    DECREASE_SCORE(state, amount = 1) {
      state.score -= Number(amount);
    },
    INCREASE_WORDAMELIORATION(state, amount = 1) {
      state.wordAmelioration += Number(amount);
    },
    DECREASE_WORDAMELIORATION(state, amount = 1) {
      state.wordAmelioration += Number(amount);
    },
    INCREASE_COSTAMELIORATION(state, payload) {
      state.ameliorations[payload.index].cost += Number(payload.amountCost);
    },
    DECREASE_COSTAMELIORATION(state, payload) {
      state.ameliorations[payload.index].cost += Number(payload.amountCost);
    },
    INCREASE_LEVELAMELIORATION(state, payload) {
      state.ameliorations[payload.index].level += Number(payload.amountLevel);
    },
    DECREASE_LEVELAMELIORATION(state, payload) {
      state.ameliorations[payload.index].level += Number(payload.amountLevel);
    },
  },
  actions: {
    updateScore({ commit }, amount) {
      if (amount >= 0) {
        commit("INCREASE_SCORE", amount);
      } else {
        commit("DECREASE_SCORE", amount);
      }
    },
    updateWordAmelioration({ commit }, amount) {
      if (amount >= 0) {
        commit("INCREASE_WORDAMELIORATION", amount);
      } else {
        commit("DECREASE_WORDAMELIORATION", amount);
      }
    },
    buyAmeliorationStore({ commit, state }, payload) {
      console.log("Buy amelioration");
      for (let i = 1; i <= payload.amountLevel; i++) {
        commit("DECREASE_SCORE", state.ameliorations[payload.index].cost);
        commit("INCREASE_LEVELAMELIORATION", {
          amountLevel: 1,
          index: payload.index,
        });
        commit("INCREASE_COSTAMELIORATION", {
          amountCost: state.ameliorations[payload.index].cost,
          index: payload.index,
        });
      }
    },
  },
  modules: {},
});
